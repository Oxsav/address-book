import * as React from 'react';

import CardAddressBook from '../../components/CardAddressBook/CardAddressBook';
import AddAddressBook from '../../components/FormAddressBook/FormAddressBook';
import Loading from '../../components/Loading/Loading';

import AddressBook from '../../entities/AddressBook';

import HttpTransport from '../../services/HttpTransport';
import LocalStorage from '../../services/LocalStorage';

import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';

import Constants from '../../constants/Constants';

import './Dashboard.scss';

/**
 * helper method to get the address books from the LocalStorate
 * @method getAddressBooks
 */
const getAddressBooks = (): AddressBook[] => {
    const addressBooks = LocalStorage.get('addressBooks');

    return addressBooks.map(
        (addressbook: AddressBook): AddressBook =>
            new AddressBook(
                addressbook.id,
                addressbook.username,
                addressbook.groups,
                addressbook.contacts
            )
    );
};

/**
 * class Dashboard component
 */
class DashBoard extends React.Component<any, any> {
    public state: any;
    public candidate: string;

    /**
     * class Dashboard constructor
     */
    constructor(props: any) {
        super(props);

        this.state = {
            addingBook: false,
            addressBooks: getAddressBooks(),
            isLoading: false,
            password: '',
            username: ''
        };
    }

    /**
     * Render an array of React.nodes the belonging contacts of group
     * @method renderContacts
     * @returns {Contact[]} array of contacts belonging to a group
     */
    public render(): React.ReactNode {
        return (
            <div className="dashboard">
                {LocalStorage.get('isLoading') || this.state.isLoading ? (
                    <Loading />
                ) : null}

                <div className="address-books">
                    {this.renderAddressBooks()}
                    {this.state.addingBook ? (
                        <AddAddressBook
                            open={this.state.addingBook}
                            onSave={this.saveAddressBook}
                            closeModal={this.stopAdding}
                        />
                    ) : null}
                </div>
                <div className="add-button">
                    <Button
                        className="add-btn"
                        variant="fab"
                        color="primary"
                        aria-label="Add"
                        onClick={this.clickAdd}
                    >
                        <AddIcon />
                    </Button>
                </div>
            </div>
        );
    }

    /**
     * Render an array of React.nodes CardAddressBook
     * @method renderAddressBooks
     * @returns {any[]} CarAddressBook component
     */
    private renderAddressBooks = (): any[] | any => {
        if (this.state.addressBooks.length === 0) {
            return <div> No address books... </div>;
        }

        return this.state.addressBooks.map(
            (addressbook: AddressBook): any => (
                <CardAddressBook
                    key={addressbook.id}
                    username={addressbook.username}
                    id={addressbook.id}
                    numberContacts={addressbook.contacts.length}
                    goTo={this.goTo}
                    onDelete={this.handleDelete}
                />
            )
        );
    };
    /**
     * Handle event click add
     * updates the addingBook state
     * @method clickAdd
     */
    private clickAdd = () => {
        this.setState({
            addingBook: true
        });
    };

    /**
     * Handle event click add
     * updates the addingBook state
     * @method clickAdd
     */
    private stopAdding = (e: any) => {
        this.setState({
            addingBook: false
        });
    };

    /**
     * Try to save an addressbook
     * @method saveAddressBook
     * @param username {string} - name of the username
     * @param password {string} - name of the password
     */
    private saveAddressBook = (
        username: string,
        password: string
    ): Promise<any> => {
        this.setState({
            isLoading: true
        });
        return HttpTransport.post(
            `${Constants.API_URL}/${this.props.candidate}`,
            {
                password,
                username
            }
        ).then((data: any) => {
            const { username: name, id, groups, contacts } = data.value;

            const addressbook = new AddressBook(id, name, groups, contacts);
            const addressbooks = getAddressBooks();
            addressbooks.push(addressbook);

            this.setState({
                addingBook: false,
                addressBooks: addressbooks
            });

            this.setState({
                isLoading: false
            });

            LocalStorage.set('addressBooks', addressbooks);
        });
    };

    /**
     * Redirects to the chosen addressbook page
     * @method goTo
     * @param id {string} - id of the addressbook
     */
    private goTo = (id: string) => {
        this.props.history.push(`/addressbook/${id}`);
    };

    /**
     * Handle event click delete, tries to delete an addressbook
     * @method handleDelete
     * @param id {string} - id of the addressbook
     */
    private handleDelete = (id: string) => {
        this.setState({
            addingBook: false,
            isLoading: true
        });
        HttpTransport.delete(
            `${Constants.API_URL}/${LocalStorage.get('candidate')}/${id}`
        ).then(() => {
            const addressBooks = getAddressBooks();

            const index = addressBooks.findIndex(
                (addressBook: AddressBook) => id === addressBook.id
            );

            addressBooks.splice(index, 1);

            this.setState({
                addressBooks,
                isLoading: false
            });

            LocalStorage.set('addressBooks', addressBooks);
        });
    };
}

export default DashBoard;
