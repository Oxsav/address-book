import * as React from 'react';
import * as ReactRouter from 'react-router-dom'; // { BrowserRouter as Router, Route, Switch }

import Authentication from './components/Authentication/Authentication';
import BookDashboard from './components/BookDashboard/BookDashboard';

import Call from './components/Call/Call';
import Dashboard from './components/Dashboard/Dashboard';
import Header from './components/Header/Header';
import ProtectedRoute from './components/ProtectedRoute/ProtectedRoute';

import * as Auth from './context/AuthContext';

import './App.css';

class App extends React.Component<{ history?: any }> {
    public readonly props: any;

    public render(): React.ReactNode {
        return (
            <ReactRouter.BrowserRouter>
                <Auth.AuthProvider>
                    <Header />
                    <ReactRouter.Switch>
                        <ProtectedRoute
                            path="/dashboard"
                            component={Dashboard}
                        />
                        <ProtectedRoute
                            path="/addressbook/:id"
                            component={BookDashboard}
                        />
                        <ReactRouter.Route path="/call/:id" component={Call} />
                        <ReactRouter.Route
                            path="/"
                            component={Authentication}
                        />
                    </ReactRouter.Switch>
                </Auth.AuthProvider>
            </ReactRouter.BrowserRouter>
        );
    }
}

export default App;
