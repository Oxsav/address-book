import * as React from 'react';
import * as ReactRouter from 'react-router-dom';

import HttpTransport from '../services/HttpTransport';
import LocalStorage from '../services/LocalStorage';

import Constants from '../constants/Constants';

interface IAuthContext {
    candidate: string;
    isAuth: boolean;
    isLoading: boolean;
    login: (candidate: string) => any;
    logout: (event: any) => any;
}

// Creates auth context
const AuthContext = React.createContext({} as IAuthContext);

/**
 * class Provider Component
 */
class Provider extends React.Component<any, any> {
    public state = { candidate: '', isAuth: false, isLoading: false };

    /**
     * Render the component to the DOM
     * @method render
     * @returns {React.Node} node to render
     */
    public render() {
        return (
            <AuthContext.Provider
                value={{
                    candidate: this.state.candidate,
                    isAuth: this.state.isAuth,
                    isLoading: this.state.isLoading,
                    login: this.login,
                    logout: this.logout
                }}
            >
                {this.props.children}
            </AuthContext.Provider>
        );
    }

    /**
     * Preform the authentication
     * @method login
     * @param candidate {string}
     * @returns {Promise<any>} promise of the login method
     */
    public login = (candidate: string): Promise<any> => {
        this.setState({ isLoading: true });

        return HttpTransport.get(`${Constants.API_URL}/${candidate}/`).then(
            (response: any) => {
                this.setState({
                    candidate,
                    isAuth: true,
                    isLoading: false
                });

                LocalStorage.set('candidate', candidate);
                LocalStorage.set('isAuth', true);
                LocalStorage.set('addressBooks', response.value);
                LocalStorage.set('loading', false);
                this.props.history.push('/dashboard');
            }
        );
    };

    /**
     * Preform the logout
     * set the state and removes localstorage data
     * @method logout
     */
    public logout = () => {
        this.setState({
            candidate: '',
            isAuth: false
        });
        LocalStorage.remove('candidate');
        LocalStorage.remove('addressBooks');
    };
}

const AuthConsumer = AuthContext.Consumer;
const AuthProvider = ReactRouter.withRouter(Provider);

export { AuthProvider, AuthConsumer };
