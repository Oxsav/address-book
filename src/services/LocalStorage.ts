/**
 * Parse json of the result
 * @method getJson
 * @param result {any} - result of the query to localstorage
 * @returns {any} - parsed data
 */
const getJson = (result: any): any => {
    return JSON.parse(result);
};

/**
 * class LocalStorage
 */
class LocalStorage {
    /**
     * Gets data of an sepecific key
     * @method get
     * @param item {string} - item to get
     */
    public get(item: string): any {
        return getJson(localStorage.getItem(item));
    }

    /**
     * Sets data of an sepecific key
     * @method set
     * @param item {string} - item to set
     * @param data {any} - data to set
     */
    public set(item: string, data: any): void {
        localStorage.setItem(item, JSON.stringify(data));
    }

    /**
     * Removes data of an sepecific key
     * @method remove
     * @param item {string} - item to remove
     */
    public remove(item: string): void {
        localStorage.removeItem(item);
    }

    /**
     * Clears all data of localstorage
     * @method clear
     */
    public clear(): void {
        localStorage.clear();
    }
}

export default new LocalStorage();
