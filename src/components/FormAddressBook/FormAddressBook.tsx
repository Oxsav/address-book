import * as React from 'react';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';

interface IAddAddressBookProps {
    open: boolean;
    closeModal: any;
    onSave: any;
}

/**
 * class AddAddressBook Component
 */
class AddAddressBook extends React.Component<IAddAddressBookProps, any> {
    public open: any;
    public closeModal: any;
    public onSave: any;

    /**
     * class AddAddressBook constructor
     * @param props
     */
    constructor(props: IAddAddressBookProps) {
        super(props);
        this.state = {
            open: props.open || false,
            password: '',
            username: ''
        };
    }

    /**
     * Render the component to the DOM
     * @method render
     * @returns {React.Node} node to render
     */
    public render(): React.ReactNode {
        return (
            <Dialog
                open={this.state.open}
                onClose={this.props.closeModal}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">
                    Add Address book
                </DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus={true}
                        margin="dense"
                        id="username"
                        label="Username"
                        type="text"
                        fullWidth={true}
                        onChange={this.updateUsername}
                    />
                    <TextField
                        margin="dense"
                        id="password"
                        label="Passord"
                        type="password"
                        fullWidth={true}
                        onChange={this.updatePassword}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.props.closeModal} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.handleSave} color="primary">
                        Add
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }

    /**
     * handle event onclick save
     * calls the props onSave function
     * @param e {any} - click event
     */
    private handleSave = (e: any): void => {
        this.props.onSave(this.state.username, this.state.password);
    };

    /**
     * handle event onchange username input
     * updates the state username
     * @method updateUsername
     * @param e {any} - change event
     */
    private updateUsername = (e: any): void => {
        this.setState({
            username: e.target.value
        });
    };

    /**
     * handle event onchange password input
     * updates the state password
     * @method updatePassword
     * @param e {any} - change event
     */
    private updatePassword = (e: any): void => {
        this.setState({
            password: e.target.value
        });
    };
}

export default AddAddressBook;
