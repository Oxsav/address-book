import * as React from 'react';
import * as ReactRouter from 'react-router-dom';
import * as Auth from '../../context/AuthContext';
import LocalStorage from '../../services/LocalStorage';

/**
 * Render the component with the context of the autentication
 * @method renderProps
 * @param Component {any} - component to render
 * @returns Component
 */
const renderProps = (Component: any) => {
    const isAuth = LocalStorage.get('isAuth');
    const candidate = LocalStorage.get('candidate');

    return (props: any) =>
        isAuth && Boolean(candidate) ? (
            <Component {...props} candidate={candidate} isAuth={isAuth} />
        ) : (
            <ReactRouter.Redirect to="/" />
        );
};

/**
 * constructor of the ProtectedRoute component
 * @method ProtectedRoute
 * @param param.component {any} - Component to render
 * @param param.path {any} - Path to the route
 * @param ...rest {any[]} - the rest of the properties
 */
const ProtectedRoute = ({
    component: Component,
    ...rest
}: {
    component: any;
    path: string;
}) => (
    <Auth.AuthConsumer>
        {() => {
            return (
                <ReactRouter.Route render={renderProps(Component)} {...rest} />
            );
        }}
    </Auth.AuthConsumer>
);

export default ProtectedRoute;
