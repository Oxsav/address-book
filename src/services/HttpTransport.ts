/**
 * Promise that will call a callback function with the correct result
 * @method fetchJson
 * @param fn {Promise<any>} - function to call with json
 */
const fetchJson = async (fn: Promise<any>) => {
    const result = await fn;
    return result.json();
};

/**
 * class HttpTransport
 */
class HttpTransport {
    /**
     * headers getter
     * @returns {any} - Object of headers
     */
    get headers() {
        return {
            'Content-Type': 'application/json; charset=utf-8'
        };
    }

    /**
     * Post data
     * @param url {string} - url to post
     * @param data {any} - object with data
     * @returns {Promise<any>}
     */
    public post(url: string, data: any): Promise<object> {
        return fetchJson(
            fetch(url, {
                body: JSON.stringify(data),
                headers: this.headers,
                method: 'POST',
                mode: 'cors'
            })
        );
    }

    /**
     * Put data
     * @param url {string} - url to post
     * @param data {any} - object with data
     * @returns {Promise<any>}
     */
    public put(url: string, data: object): Promise<object> {
        return fetchJson(
            fetch(url, {
                body: JSON.stringify(data),
                headers: this.headers,
                method: 'PUT',
                mode: 'cors'
            })
        );
    }

    /**
     * Delete data
     * @param url {string} - url to post
     * @returns {Promise<any>}
     */
    public delete(url: string): Promise<object> {
        return fetchJson(
            fetch(url, {
                headers: this.headers,
                method: 'DELETE',
                mode: 'cors'
            })
        );
    }

    /**
     * Get data
     * @param url {string} - url to post
     * @returns {Promise<any>}
     */
    public get(url: string): Promise<object> {
        return fetchJson(fetch(url, { headers: this.headers }));
    }
}

export default new HttpTransport();
