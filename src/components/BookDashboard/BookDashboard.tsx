import * as React from 'react';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';

import CallIcon from '@material-ui/icons/Call';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import PersonAddIcon from '@material-ui/icons/PersonAdd';

import Alert from '../Alert/Alert';
import FormAddContact from '../FormAddContact/FormAddContact';
import FormAddGroup from '../FormAddGroup/FormAddGroup';
import Loading from '../Loading/Loading';

import Constants from '../../constants/Constants';

import AddressBook from '../../entities/AddressBook';
import ContactGroup from '../../entities/ContactGroup';
import HttpTransport from '../../services/HttpTransport';

import './BookDashboard.scss';

/**
 * class Component BookDashboard
 */
class BookDashboard extends React.Component<any, any> {
    /**
     * class BookDashboard Constructor
     * @constructor
     * @param props
     */
    constructor(props: any) {
        super(props);
        this.state = {
            addingContact: false,
            addingGroup: false,
            book: {},
            contacts: [],
            editingContact: false,
            editingGroup: false,
            error: {
                hasError: false,
                message: ''
            },
            retrieving: false,
            selectedContact: {},
            selectedGroup: {}
        };
    }

    /**
     * Hook method that is called after the component is mounted
     * Initializes the configuration
     * @method componentDidMount
     */
    public componentDidMount() {
        this.initConfiguration();
    }

    /**
     * Render the component to the DOM
     * @method render
     * @returns {React.Node} node to render
     */
    public render(): React.ReactNode {
        if (this.state.retrieving) {
            return <Loading />;
        }

        if (this.state.error.hasError) {
            return (
                <Alert
                    message={this.state.error.message}
                    onClose={this.closeAlert}
                />
            );
        }

        return (
            <div className="main">
                <div className="sidebar">
                    <div className="add-group">
                        <Button color="primary" onClick={this.clickAddGroup}>
                            <GroupAddIcon />
                        </Button>
                    </div>
                    <div className="list-groups">
                        <List>{this.renderGroups(this.state.book.groups)}</List>
                    </div>
                </div>
                <div className="contacts">
                    <div className="card">
                        <div className="actions">
                            <Button
                                color="primary"
                                onClick={this.clickAddContact}
                            >
                                <PersonAddIcon />
                            </Button>
                            <Button color="primary" onClick={this.editGroup}>
                                <EditIcon />
                            </Button>
                            <Button color="primary" onClick={this.deleteGroup}>
                                <DeleteIcon />
                            </Button>
                            <Divider />
                        </div>
                        <div className="contacts-list">
                            {this.renderContacts()}
                        </div>
                    </div>
                </div>
                {this.state.addingGroup || this.state.editingGroup ? (
                    <FormAddGroup
                        open={this.state.editingGroup || this.state.addingGroup}
                        selectedGroup={
                            this.state.editingGroup
                                ? this.state.selectedGroup
                                : {}
                        }
                        onSave={this.saveGroup}
                        closeModal={this.stopAdding}
                    />
                ) : null}
                {this.state.addingContact || this.state.editingContact ? (
                    <FormAddContact
                        groupid={this.state.selectedGroup.id}
                        selectedContact={this.state.selectedContact}
                        open={
                            this.state.addingContact ||
                            this.state.editingContact
                        }
                        onSave={this.saveContact}
                        closeModal={this.stopAdding}
                    />
                ) : null}
            </div>
        );
    }

    private closeAlert = () => {
        this.setState({
            error: {
                hasError: false,
                message: ''
            }
        });
    };

    /**
     * Called everytime a new configuration is saved to the API
     * @method initConfiguration
     */
    private initConfiguration = () => {
        this.setState({ retrieving: true });
        this.getAddressBookInfo(this.props.match.params.id).then(
            (addressBook: AddressBook) => {
                const selectedGroup = this.state.selectedGroup.hasOwnProperty(
                    'id'
                )
                    ? addressBook.groups.find(
                          group => group.id === this.state.selectedGroup.id
                      )
                    : addressBook.groups[0];

                this.setState({
                    addingContact: false,
                    addingGroup: false,
                    book: new AddressBook(
                        addressBook.id,
                        addressBook.username,
                        addressBook.groups,
                        addressBook.contacts
                    ),
                    contacts: selectedGroup
                        ? ContactGroup.getContacts(
                              addressBook.contacts,
                              selectedGroup.id
                          )
                        : [],
                    editingContact: false,
                    editingGroup: false,
                    retrieving: false,
                    selectedGroup
                });
            }
        );
    };
    /**
     * Render methods
     */

    /**
     * Render an array of React.nodes the belonging contacts of group
     * @method renderContacts
     * @returns {Contact[]} array of contacts belonging to a group
     */
    private renderContacts() {
        const { contacts } = this.state;
        if (contacts.length === 0) {
            return <div> No Contacts..</div>;
        }

        return contacts.map((contact: Contact, index: number) => (
            <Card key={index} className="contact-card">
                <CardActionArea className="card-content">
                    <CardContent>
                        <Avatar alt="name" src={contact.pictureUrl} />
                        <Typography
                            gutterBottom={true}
                            variant="headline"
                            component="h2"
                        >
                            {contact.name}
                        </Typography>
                        {contact.phone}
                    </CardContent>
                </CardActionArea>
                <CardActions className="card-actions">
                    <Button
                        size="small"
                        color="primary"
                        onClick={this.call(contact.phone)}
                    >
                        <CallIcon />
                    </Button>
                    <Button
                        size="small"
                        color="primary"
                        onClick={this.editContact(contact)}
                    >
                        <EditIcon />
                    </Button>
                    <Button
                        size="small"
                        color="primary"
                        onClick={this.deleteContact(contact.name)}
                    >
                        <DeleteIcon />
                    </Button>
                </CardActions>
            </Card>
        ));
    }

    private call = (phone: any) => (e: any) => {
        this.props.history.push(`/call/${phone}`);
    };

    /**
     * Render an array of React.nodes, the belonging groups of an addressbook
     * @method renderGroups
     * @returns {ContactGroup[]} array of groups belonging to an addressbook
     */
    private renderGroups = (groups: ContactGroup[] = []) => {
        if (groups.length === 0) {
            return (
                <ListItem button={true}>
                    <ListItemText primary="No groups" />
                </ListItem>
            );
        }
        return groups.map(group => (
            <Tooltip title={group.description} key={group.id}>
                <ListItem
                    button={true}
                    selected={this.state.selectedGroup.id === group.id}
                    onClick={this.selectGroup(group)}
                >
                    <Avatar alt="name" src={group.pictureUrl} />
                    <ListItemText primary={group.name} />
                </ListItem>
            </Tooltip>
        ));
    };

    /**
     * Events
     */

    /**
     * Set editing group state to true when trying to edit an existing group
     * @method editGroup
     */
    private editGroup = (e: any) => {
        this.setState({
            editingGroup: true
        });
    };

    /**
     * Set editing contact state to true when trying to edit an existing contact
     * @method editContact
     */
    private editContact = (contact: Contact) => (e: any) => {
        this.setState({
            editingContact: true,
            selectedContact: contact
        });
    };

    /**
     * Select a new group and its contacts
     * @method selectGroup
     */
    private selectGroup = (group: ContactGroup) => (e: any) => {
        const { contacts } = this.state.book;
        this.setState({
            contacts: ContactGroup.getContacts(contacts, group.id),
            selectedGroup: group
        });
    };

    /**
     * Set adding group state to true when trying to add a group
     * @method clickAddGroup
     */
    private clickAddGroup = () => {
        this.setState({
            addingGroup: true
        });
    };

    /**
     * Set adding contact state to true when trying to add a contact
     * @method clickAddContact
     */
    private clickAddContact = () => {
        this.setState({
            addingContact: true
        });
    };

    /**
     * Set all state variables related to editing and adding to false
     * @method stopAdding
     */
    private stopAdding = (e: any) => {
        this.setState({
            addingContact: false,
            addingGroup: false,
            editingContact: false,
            editingGroup: false
        });
    };

    /**
     * Actions
     */

    /**
     * Perform an action of saving a contact to the API
     * when success it retrieves the new configuration
     * @method saveContact
     */
    private saveContact = (
        groupid: string,
        name: string,
        phone: string,
        pictureUrl: string
    ): void => {
        this.setState({
            retrieving: true
        });
        this.addContact(groupid, name, phone, pictureUrl).then(
            (contact: Contact) => {
                if (this.state.addingContact) {
                    this.state.book.addContact(contact);
                }

                const newBook = this.state.book;

                this.initConfiguration();

                this.setState({
                    addingContact: false,
                    book: newBook,
                    contacts: ContactGroup.getContacts(
                        newBook.contacts,
                        this.state.selectedGroup.id
                    ),
                    editingContact: false
                });
            }
        );
    };

    /**
     * Perform an action of saving a group to the API
     * when success it retrieves the new configuration
     * @method saveGroup
     */
    private saveGroup = (
        id: string,
        name: string,
        description: string,
        pictureUrl: string
    ): void => {
        this.saveContactGroup(id, name, description, pictureUrl).then(
            (group: ContactGroup) => {
                const book = this.state.book;
                if (this.state.addingGroup) {
                    book.addGroup(group);
                }
                this.setState({ selectedGroup: group });

                this.initConfiguration();
            }
        );
    };

    /**
     * API actions
     */

    /**
     * Helper function that executes the action of getting information of an addressbook
     * @method getAddressBookInfo
     */
    private getAddressBookInfo = (id: string) => {
        return HttpTransport.get(
            `${Constants.API_URL}/${this.props.candidate}/${id}`
        ).then(this.handleResponse);
    };

    private handleResponse = (response: any) => {
        if (!response.success) {
            this.setState({
                error: {
                    hasError: true,
                    message: 'An error has occured'
                }
            });
        }

        return response.value;
    };

    /**
     * Helper function that executes the action of saving a group in the API
     * @method saveContactGroup
     */
    private saveContactGroup = (
        id: string,
        name: string,
        description: string,
        pictureUrl: string
    ) => {
        let action = 'post';
        let url = `${Constants.API_URL}/${this.props.candidate}/${
            this.props.match.params.id
        }/groups`;

        if (this.state.editingGroup) {
            action = 'put';
            url = `${url}/${this.state.selectedGroup.id}`;
        }

        return HttpTransport[action](url, {
            description,
            id,
            name,
            pictureUrl
        }).then(this.handleResponse);
    };

    /**
     * Helper function that executes the action of saving a contact in the API
     * @method addContact
     */
    private addContact = (
        groupId: string,
        name: string,
        phone: string,
        pictureUrl: string
    ) => {
        let action = 'post';
        let url = `${Constants.API_URL}/${this.props.candidate}/${
            this.props.match.params.id
        }/contacts`;

        if (this.state.editingContact) {
            action = 'put';
            url = `${url}/${this.state.selectedContact.name}`;
        }

        return HttpTransport[action](url, {
            groupId,
            name,
            phone,
            pictureUrl
        }).then(this.handleResponse);
    };

    /**
     * executes the action of deleting a contact in the API
     * @method addContact
     */
    private deleteContact = (name: string) => (e: any) => {
        this.setState({ retrieving: true });

        return HttpTransport.delete(
            `${Constants.API_URL}/${this.props.candidate}/${
                this.props.match.params.id
            }/contacts/${name}/`
        )
            .then(this.handleResponse)
            .then(() => {
                const book = this.state.book;
                book.removeContact(name);
                this.setState({
                    book,
                    contacts: ContactGroup.getContacts(
                        book.contacts,
                        this.state.selectedGroup.id
                    ),
                    retrieving: false
                });
            })
            .catch(() => {
                console.log('catch');
                this.setState({
                    error: {
                        hasError: true,
                        message: 'An error has occured!'
                    }
                });
            });
    };

    /**
     * executes the action of deleting a group in the API
     * @method deleteGroup
     */
    private deleteGroup = (e: any) => {
        return HttpTransport.delete(
            `${Constants.API_URL}/${this.props.candidate}/${
                this.props.match.params.id
            }/groups/${this.state.selectedGroup.id}/`
        )
            .then(this.handleResponse)
            .then(() => {
                const { selectedGroup } = this.state;

                const book = this.state.book;
                book.removeGroup(selectedGroup.id);
                console.log('BOOK ', book.groups);
                const group = book.groups[0] ? book.groups[0] : {};

                this.setState({
                    book,
                    contacts: ContactGroup.getContacts(book.contacts, group.id),
                    selectedGroup: group
                });
            })
            .catch(() =>
                this.setState({
                    error: {
                        hasError: true,
                        message: 'An error has occured!'
                    }
                })
            );
    };
}

export default BookDashboard;
