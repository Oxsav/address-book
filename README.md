## Use node 10 (install nvm)

```js
nvm use
```

## Install dependencies

```js
npm install
```

## Run locally dependencies

```js
npm run start
```

## Run locally dependencies with HTTPS

```js
HTTPS=true npm run start
```

## Build application to prod

```js
npm run build
```
