import * as React from 'react';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import * as Auth from '../../context/AuthContext';

import Loading from '../../components/Loading/Loading';

import LocalStorage from '../../services/LocalStorage';

import './Authentication.scss';

interface IAuthenticationProps {
    login: (candidate: string) => any;
    logout: (event: any) => any;
    history: any;
}

/**
 * class Component Authentication
 */
class Authentication extends React.Component<IAuthenticationProps, any> {
    public state: any;

    /**
     * class Authentication constructor
     * @param props
     */
    constructor(props: IAuthenticationProps) {
        super(props);
        this.state = {
            candidate: ''
        };
    }

    /**
     * Hook method that is called after the component is mounted
     * Verifies if candidate exists
     * @method componentDidMount
     */
    public componentDidMount() {
        if (LocalStorage.get('candidate')) {
            this.props.history.push('/dashboard');
        }
    }

    /**
     * Render the component to the DOM
     * @method render
     * @returns {React.Node} node to render
     */
    public render(): React.ReactNode {
        return (
            <Auth.AuthConsumer>
                {({ isLoading, login }: { isLoading: boolean; login: any }) => (
                    <div className="authentication">
                        <form
                            onSubmit={this.authenticate(login)}
                            className="form-auth"
                        >
                            <div className="auth-header">
                                <h2>Authenticate</h2>
                            </div>
                            <div className="auth-fields">
                                <div className="auth-candidate">
                                    <TextField
                                        required={true}
                                        id="standard-required"
                                        label="Candidate"
                                        margin="normal"
                                        onChange={this.onChange}
                                        fullWidth={true}
                                    />
                                </div>
                                <div className="submit-auth">
                                    <Button
                                        variant="raised"
                                        color="primary"
                                        type="submit"
                                    >
                                        Submit
                                    </Button>
                                </div>
                            </div>
                        </form>
                        {isLoading ? <Loading /> : null}
                    </div>
                )}
            </Auth.AuthConsumer>
        );
    }

    /**
     * handle onChange updates the state of the candidate
     * @method onChange
     * @param e - event on change
     */
    private onChange = (e: any) => {
        this.setState({
            candidate: e.target.value
        });
    };

    /**
     * Calls the login method that is related to the consumer
     * @method authenticate
     * @param e - event on change
     * @returns the event handler function
     */
    private authenticate = (login: any) => (e: any) => {
        e.preventDefault();
        login(this.state.candidate);
    };
}

export default Authentication;
