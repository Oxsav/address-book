import * as React from 'react';

import Avatar from '@material-ui/core/Avatar';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import MoreVertIcon from '@material-ui/icons/MoreVert';

import './CardAddressBook.scss';

interface ICardAddressBook {
    username: string;
    id: string;
    numberContacts: number;
    goTo: any;
    onDelete: any;
}

/**
 * class Component CardAddressBook
 */
class CardAddressBook extends React.Component<ICardAddressBook, any> {
    constructor(props: ICardAddressBook) {
        super(props);
        this.state = {
            anchorEl: null
        };
    }

    /**
     * Render an array of React.nodes the belonging contacts of group
     * @method renderContacts
     * @returns {Contact[]} array of contacts belonging to a group
     */
    public render(): React.ReactNode {
        const { anchorEl } = this.state;

        return (
            <Card className="address-book">
                <CardHeader
                    avatar={
                        <Avatar aria-label="Recipe">
                            {this.props.username
                                .charAt(0)
                                .toLocaleUpperCase() || 'An'}
                        </Avatar>
                    }
                    action={
                        <div>
                            <IconButton
                                aria-owns={anchorEl ? 'simple-menu' : undefined}
                                aria-haspopup="true"
                                onClick={this.handleClick}
                            >
                                <MoreVertIcon />
                            </IconButton>
                            <Menu
                                id="simple-menu"
                                anchorEl={anchorEl}
                                open={Boolean(anchorEl)}
                                onClose={this.handleClose}
                            >
                                <MenuItem onClick={this.goTo}>
                                    See Address Book
                                </MenuItem>
                                <MenuItem onClick={this.deleteAddressBook}>
                                    Delete
                                </MenuItem>
                            </Menu>
                        </div>
                    }
                    title={this.props.username}
                    subheader={this.props.id}
                />
                <CardContent>{this.props.numberContacts} Contacts</CardContent>
            </Card>
        );
    }

    /**
     * handle event click for the menu
     * updates the anchorEl
     * @method handleClick
     */
    private handleClick = (event: any) => {
        this.setState({ anchorEl: event.currentTarget });
    };

    /**
     * handle event click for the menu to close
     * set anchorEl to null
     * @method handleClose
     */
    private handleClose = () => {
        this.setState({ anchorEl: null });
    };

    /**
     * handle event delete address book
     * calls the ondelete method passed throught the props
     * @method deleteAddressBook
     */
    private deleteAddressBook = () => {
        this.setState({ anchorEl: null });
        this.props.onDelete(this.props.id);
    };

    /**
     * handle event click when selecting an address book
     * calls the props goTo method
     * @method goTo
     */
    private goTo = () => {
        this.props.goTo(this.props.id);
    };
}

export default CardAddressBook;
