import ContactGroup from './ContactGroup';

interface IAddressBookInterface {
    username: string;
    id: string;
    groups: ContactGroup[];
    contacts: Contact[];
}

/**
 * class AddressBook
 */
class AddressBook implements IAddressBookInterface {
    public id: string;
    public username: string;
    public password: string;
    public groups: ContactGroup[];
    public contacts: Contact[];

    /**
     * class Addressbook constructor
     * @param id {string} - id of the Addressbook
     * @param username {string} - username of the Addressbook
     * @param groups {string} - groups of the Addressbook
     * @param contacts {string} - id of the Addressbook
     */
    constructor(
        id: string,
        username: string,
        groups: ContactGroup[],
        contacts: Contact[]
    ) {
        this.username = username;
        this.id = id;
        this.groups = groups;
        this.contacts = contacts;
    }

    /**
     * Adds group to the groups addressbook
     * @method addGroup
     * @param group {ContactGroup}
     */
    public addGroup(group: ContactGroup) {
        this.groups.push(group);
    }

    /**
     * Adds contact to the contacts addressbook
     * @method addContact
     * @param contact {Contact}
     */
    public addContact(contact: Contact) {
        this.contacts.push(contact);
    }

    /**
     * Removes group from groups addressbook
     * @method removeGroup
     * @param groupid {string}
     */
    public removeGroup(groupid: string) {
        this.groups = this.groups.filter(
            (group: ContactGroup) => group.id !== groupid
        );
    }

    /**
     * Removes contact from contacts addressbook
     * @method removeContact
     * @param contactName {string}
     */
    public removeContact(contactName: string) {
        this.contacts = this.contacts.filter(
            (contact: Contact) => contact.name !== contactName
        );
    }
}

export default AddressBook;
