import * as React from 'react';
import { Link } from 'react-router-dom';
import * as Auth from '../../context/AuthContext';

import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';

import AccountCircle from '@material-ui/icons/AccountCircle';
import ExitIcon from '@material-ui/icons/ExitToApp';

import LocalStorage from '../../services/LocalStorage';

import './Header.scss';

/**
 * class Header Component
 */
class Header extends React.Component<any, any> {
    /**
     * class Header constructor
     * @param props
     */
    constructor(props: any) {
        super(props);

        this.state = {
            anchorEl: null
        };
    }

    /**
     * Render the component to the DOM
     * @method render
     * @returns {React.Node} node to render
     */
    public render() {
        const { anchorEl } = this.state;
        const open = Boolean(anchorEl);

        return (
            <Auth.AuthConsumer>
                {({ logout }: { logout: any }) => {
                    const isAuth = LocalStorage.get('isAuth');
                    const candidate = LocalStorage.get('candidate');

                    return (
                        <AppBar position="static" className="header">
                            <Toolbar>
                                <div>
                                    <Typography variant="title" color="inherit">
                                        <Link
                                            className="header-logo"
                                            to={`/dashboard`}
                                        >
                                            AddressBook
                                        </Link>
                                    </Typography>
                                </div>
                                {candidate &&
                                    isAuth && (
                                        <div className="profile">
                                            <IconButton
                                                aria-owns={
                                                    open
                                                        ? 'menu-appbar'
                                                        : undefined
                                                }
                                                aria-haspopup="true"
                                                onClick={this.handleMenu}
                                                color="inherit"
                                            >
                                                <AccountCircle />
                                            </IconButton>
                                            <Menu
                                                id="menu-appbar"
                                                anchorEl={anchorEl}
                                                anchorOrigin={{
                                                    horizontal: 'right',
                                                    vertical: 'top'
                                                }}
                                                transformOrigin={{
                                                    horizontal: 'right',
                                                    vertical: 'top'
                                                }}
                                                open={open}
                                                onClose={this.handleClose}
                                            >
                                                <MenuItem>{candidate}</MenuItem>
                                                <MenuItem
                                                    onClick={this.handleLogout(
                                                        logout
                                                    )}
                                                >
                                                    <Tooltip title="Logout">
                                                        <ExitIcon />
                                                    </Tooltip>
                                                </MenuItem>
                                            </Menu>
                                        </div>
                                    )}
                            </Toolbar>
                        </AppBar>
                    );
                }}
            </Auth.AuthConsumer>
        );
    }

    /**
     * handle event onclick menu
     * updates the state anchorEl
     * @method handleMenu
     * @param event {any} - change event
     */
    private handleMenu = (e: any) => {
        this.setState({ anchorEl: e.currentTarget });
    };

    /**
     * handle event onclose menu
     * updates the state anchorEl
     * @method handleClose
     */
    private handleClose = () => {
        this.setState({ anchorEl: null });
    };

    /**
     * handle event on click logout
     * updates the state anchorEl
     * @method handleLogout
     * @param logout {any} function to logout
     */
    private handleLogout = (logout: any) => (e: any) => {
        this.setState({ anchorEl: null });
        logout();
    };
}
export default Header;
