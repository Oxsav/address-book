import * as React from 'react';

import CircularProgress from '@material-ui/core/CircularProgress';
import Fade from '@material-ui/core/Fade';

import './Loading.scss';

/**
 * Constructor of Loading component
 * @method Loading
 */
const Loading = () => (
    <div className="loading">
        <Fade in={true}>
            <CircularProgress />
        </Fade>
    </div>
);

export default Loading;
