import * as React from 'react';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';

import { Guid } from 'guid-typescript';
import ContactGroup from '../../entities/ContactGroup';

interface IFromAddGroupProps {
    open: boolean;
    closeModal: any;
    onSave: any;
    selectedGroup: ContactGroup;
}

/**
 * class FromAddGroup Component
 */
class FromAddGroup extends React.Component<IFromAddGroupProps, any> {
    public open: any;
    public selectedGroup: ContactGroup;
    public closeModal: any;
    public onSave: any;

    /**
     * class FromAddGroup constructor
     * @param props
     */
    constructor(props: IFromAddGroupProps) {
        super(props);
        const id = Guid.create();
        this.state = {
            description: props.selectedGroup.description || '',
            id: props.selectedGroup.id || id.toString(),
            name: props.selectedGroup.name || '',
            open: props.open || false,
            pictureUrl: props.selectedGroup.pictureUrl || ''
        };
    }

    /**
     * Render the component to the DOM
     * @method render
     * @returns {React.Node} node to render
     */
    public render(): React.ReactNode {
        return (
            <Dialog
                open={this.state.open}
                onClose={this.props.closeModal}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">
                    Add Group Contact
                </DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus={true}
                        margin="dense"
                        id="name"
                        label="Name"
                        type="text"
                        fullWidth={true}
                        value={this.state.name}
                        onChange={this.updateName}
                    />
                    <TextField
                        margin="dense"
                        id="description"
                        label="Description"
                        type="text"
                        fullWidth={true}
                        value={this.state.description}
                        onChange={this.updateDescription}
                    />
                    <TextField
                        margin="dense"
                        id="pictureUrl"
                        label="Picture URL"
                        type="text"
                        fullWidth={true}
                        value={this.state.pictureUrl}
                        onChange={this.updatePictureUrl}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.props.closeModal} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.handleSave} color="primary">
                        Add
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }

    /**
     * handle event onclick save
     * calls the props onSave function
     *
     * @param e {any} - click event
     */
    private handleSave = (e: any): void => {
        this.props.onSave(
            this.state.id,
            this.state.name,
            this.state.description,
            this.state.pictureUrl
        );
    };

    /**
     * handle event onchange name input
     * updates the state name
     * @method updateName
     * @param e {any} - change event
     */
    private updateName = (e: any): void => {
        this.setState({
            name: e.target.value
        });
    };

    /**
     * handle event onchange description input
     * updates the state description
     * @method updateDescription
     * @param e {any} - change event
     */
    private updateDescription = (e: any): void => {
        this.setState({
            description: e.target.value
        });
    };

    /**
     * handle event onchange pictureUrl input
     * updates the state pictureUrl
     * @method updatePictureUrl
     * @param e {any} - change event
     */
    private updatePictureUrl = (e: any): void => {
        this.setState({
            pictureUrl: e.target.value
        });
    };
}

export default FromAddGroup;
