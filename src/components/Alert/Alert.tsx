import * as React from 'react';

import IconButton from '@material-ui/core/IconButton';
import Snackbar from '@material-ui/core/Snackbar';

import CloseIcon from '@material-ui/icons/Close';

interface ISnackbarProps {
    message: string;
    onClose: any;
}

/**
 * class AddAddressBook Component
 */
class Alert extends React.Component<ISnackbarProps, any> {
    /**
     * class AddAddressBook constructor
     * @param props
     */
    constructor(props: ISnackbarProps) {
        super(props);
        this.state = {
            open: true
        };
    }

    /**
     * Render the component to the DOM
     * @method render
     * @returns {React.Node} node to render
     */
    public render(): React.ReactNode {
        return (
            <Snackbar
                anchorOrigin={{
                    horizontal: 'center',
                    vertical: 'top'
                }}
                open={this.state.open}
                autoHideDuration={6000}
                onClose={this.handleClose}
                ContentProps={{
                    'aria-describedby': 'message-id'
                }}
                message={this.props.message}
                action={[
                    <IconButton
                        key="close"
                        aria-label="Close"
                        color="inherit"
                        onClick={this.handleClose}
                    >
                        <CloseIcon />
                    </IconButton>
                ]}
            />
        );
    }

    /**
     * handle close event
     * calls onClose method
     * @param e {any} - click event
     */
    private handleClose = (e: any): void => {
        this.setState({
            open: false
        });
        this.props.onClose();
    };
}

export default Alert;
