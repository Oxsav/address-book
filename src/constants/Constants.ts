/**
 * class Constants
 * have the constants that the application needs
 */
class Constants {
    get API_URL() {
        return 'http://frontend-addressbook.herokuapp.com';
    }
    
    get SIGNALLING_SERVER() {
        return 'ws://signaling-server-address.herokuapp.com/';
    }
}

export default new Constants();
