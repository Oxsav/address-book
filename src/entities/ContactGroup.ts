/**
 * class ContactGroup
 */
class ContactGroup {
    /**
     * Static method to filter the contacts by group
     * @method getContacts
     * @param contacts
     * @param id
     */
    public static getContacts(contacts: Contact[], id: string) {
        return contacts.filter(contact => contact.groupId === id);
    }

    public id: string;
    public name: string;
    public description: string;
    public pictureUrl: string;

    /**
     *
     * @param id {string} - id of the ContactGroup
     * @param name {string} - name of the ContactGroup
     * @param description {string} - description of the ContactGroup
     * @param pictureUrl {string} - picture url of the ContactGroup
     */
    constructor(
        id: string,
        name: string,
        description: string,
        pictureUrl: string
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.pictureUrl = pictureUrl;
    }
}

export default ContactGroup;
