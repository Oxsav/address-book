import * as React from 'react';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';

import ContactGroup from '../../entities/ContactGroup';

interface IFromAddGroupProps {
    open: boolean;
    groupid: ContactGroup[];
    closeModal: any;
    onSave: any;
    selectedContact: Contact;
}

/**
 * class FormAddContact Component
 */
class FromAddGroup extends React.Component<IFromAddGroupProps, any> {
    public open: any;
    public groups: any;
    public closeModal: any;
    public onSave: any;

    /**
     * class FormAddContact constructor
     * @param props
     */
    constructor(props: IFromAddGroupProps) {
        super(props);
        this.state = {
            groupid: props.groupid,
            name: this.props.selectedContact.name || '',
            open: props.open || false,
            phone: this.props.selectedContact.phone || '',
            pictureUrl: this.props.selectedContact.pictureUrl || ''
        };
    }

    /**
     * Render the component to the DOM
     * @method render
     * @returns {React.Node} node to render
     */
    public render(): React.ReactNode {
        return (
            <Dialog
                open={this.state.open}
                onClose={this.props.closeModal}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Add Contact</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus={true}
                        margin="dense"
                        id="name"
                        label="Name"
                        type="text"
                        fullWidth={true}
                        value={this.state.name}
                        onChange={this.updateName}
                    />
                    <TextField
                        margin="dense"
                        id="phone"
                        label="Phone"
                        type="text"
                        fullWidth={true}
                        value={this.state.phone}
                        onChange={this.updatePhone}
                    />
                    <TextField
                        margin="dense"
                        id="pictureUrl"
                        label="Picture URL"
                        type="text"
                        fullWidth={true}
                        value={this.state.pictureUrl}
                        onChange={this.updatePictureUrl}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.props.closeModal} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.handleSave} color="primary">
                        Add
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }

    /**
     * handle event onclick save
     * calls the props onSave function
     * @param e {any} - click event
     */
    private handleSave = (e: any): void => {
        this.props.onSave(
            this.state.groupid,
            this.state.name,
            this.state.phone,
            this.state.pictureUrl
        );
    };

    /**
     * handle event onchange name input
     * updates the state name
     * @method updateName
     * @param e {any} - change event
     */
    private updateName = (e: any): void => {
        this.setState({
            name: e.target.value
        });
    };

    /**
     * handle event onchange phone input
     * updates the state phone
     * @method updatePhone
     * @param e {any} - change event
     */
    private updatePhone = (e: any): void => {
        this.setState({
            phone: e.target.value
        });
    };

    /**
     * handle event onchange pictureUrl input
     * updates the state pictureUrl
     * @method updatePictureUrl
     * @param e {any} - change event
     */
    private updatePictureUrl = (e: any): void => {
        this.setState({
            pictureUrl: e.target.value
        });
    };
}

export default FromAddGroup;
