import * as React from 'react';

import { Guid } from 'guid-typescript';

import Constants from '../../constants/Constants';

import './Call.scss';

const MESSAGE_TYPES = {
    CLOSECONNECTION: 'CLOSECONNECTION',
    CREATEANSWER: 'CREATEANSWER',
    CREATEOFFER: 'CREATEOFFER',
    ICECANDIDATE: 'ICECANDIDATE'
};

const servers = {
    iceServers: [
        {
            urls: 'stun:stun.l.google.com:19302'
        },
        {
            credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
            urls: 'turn:192.158.29.39:3478?transport=udp',
            username: '28224511:1379330808'
        },
        {
            credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
            urls: 'turn:192.158.29.39:3478?transport=tcp',
            username: '28224511:1379330808'
        }
    ]
};

class Call extends React.Component<any, any> {
    public state: any;
    public candidate: string;
    public video: any;
    public stream: any;
    public remoteStream: any;
    public remoteVideo: any;
    public userID: string;
    public pc: any;
    public pc2: any;
    public socket: any;
    public servers: any;
    public peers: any;
    public peerConnections: any;
    public remoteContainer: any;

    constructor(props: any) {
        super(props);
        this.userID = Guid.create().toString();
        this.peerConnections = {};
        this.remoteStream = {};
        this.remoteVideo = {};
        this.peers = [];
        this.state = {
            offerToReceiveAudio: 1,
            offerToReceiveVideo: 1,
            peers: [],
            refLocalVideo: React.createRef(),
            refRemoveVideo: React.createRef()
        };
    }

    public componentDidMount() {
        window.addEventListener('beforeunload', () => {
            this.peers.forEach((peer: string) =>
                this.socket.send(
                    JSON.stringify({
                        from: this.userID,
                        to: peer,
                        type: MESSAGE_TYPES.CLOSECONNECTION
                    })
                )
            );

            this.socket.close(
                1000,
                `${this.userID},${this.props.match.params.id}`
            );
            window.removeEventListener('beforeunload', () => {
                this.socket.close(
                    1000,
                    `${this.userID},${this.props.match.params.id}`
                );
            });
        });

        this.socket = new WebSocket(Constants.SIGNALLING_SERVER);

        this.socket.addEventListener('open', (event: any) => {
            console.log('open');
            this.start();
        });

        this.socket.addEventListener('message', (event: any) => {
            this.onServerMessage(JSON.parse(event.data));
        });
    }

    public render(): React.ReactNode {
        return (
            <div className="call-dashboard">
                <div id="local-video">
                    <video
                        height="400px"
                        ref={this.setVideoRef}
                        id="localVideo"
                        muted={true}
                        autoPlay={true}
                    />
                </div>
                <div className="remote-videos">
                    {this.state.peers.map((elm: any) => (
                        <div className="remote-video">
                            <video
                                key={elm}
                                ref={this.setRemoveVideoRef(elm)}
                                className="remote-video"
                                autoPlay={true}
                            />
                        </div>
                    ))}
                </div>
            </div>
        );
    }

    private setRemoteDescription = (message: any) => {
        if (!this.peerConnections[message.from]) {
            this.peerConnections = {
                [message.from]: new RTCPeerConnection(servers)
            };
            this.setState({
                peers: [...this.state.peers, message.from]
            });
            this.remoteStream[message.from] = {};
            this.peerConnections[message.from].onaddstream = (e: any) =>
                this.handleTrackEvent(message.from, this.remoteStream)(e);
            this.peerConnections[
                message.from
            ].onicecandidate = this.onIceCandidate(message.from, message.to);
            this.peerConnections[message.from].oniceconnectionstatechange = (
                e: any
            ) => this.onIceStateChange(this.peerConnections[message.from]);
            this.stream
                .getTracks()
                .forEach((track: any) =>
                    this.peerConnections[message.from].addTrack(
                        track,
                        this.stream
                    )
                );
        }

        this.peerConnections[message.from]
            .setRemoteDescription(message.description)
            .then(
                () =>
                    this.onSetRemoteSuccess(this.peerConnections[message.from]),
                this.onSetSessionRemoteOfferDescriptionError
            );

        this.peerConnections[message.from]
            .createAnswer()
            .then(
                (desc: any) => this.onCreateAnswerSuccess(desc, message),
                this.onCreateAnswerSessionDescriptionError
            );
    };

    private onServerMessage = (message: any) => {
        if (message.from === 'server') {
            if (message.clients.length <= 0) {
                return;
            }

            this.peers = message.clients;

            this.setState({
                peers: message.clients
            });

            this.peers.forEach((peer: string) => {
                this.peerConnections[peer] = new RTCPeerConnection(servers);
                this.remoteStream[peer] = {};
                this.peerConnections[peer].onaddstream = this.handleTrackEvent(
                    peer,
                    this.remoteStream
                );
                this.stream
                    .getTracks()
                    .forEach((track: any) =>
                        this.peerConnections[peer].addTrack(track, this.stream)
                    );

                this.peerConnections[peer].onicecandidate = this.onIceCandidate(
                    this.userID,
                    peer
                );
                this.peerConnections[peer]
                    .createOffer(this.state.offerOptions)
                    .then(
                        (desc: any) => this.onCreateOfferSuccess(desc, peer),
                        this.onCreateOfferSessionDescriptionError
                    );
            });
            return;
        }

        switch (message.type) {
            case MESSAGE_TYPES.CREATEANSWER:
            case MESSAGE_TYPES.CREATEOFFER:
                this.setRemoteDescription(message);
                return;
            case MESSAGE_TYPES.ICECANDIDATE:
                this.setCandidate(message);
                return;
            case MESSAGE_TYPES.CLOSECONNECTION:
                this.closeConnection(message);
                return;
            default:
                return;
        }
    };

    private closeConnection = (message: any) => {
        this.peerConnections[message.from].close();
        const newPeers = this.peers.splice(this.peers.indexOf(message.from), 1);
        this.setState({
            peers: newPeers
        });
    };

    private setCandidate = (message: any) => {
        this.peerConnections[message.from]
            .addIceCandidate(message.candidate)
            .then(
                () =>
                    this.onAddIceCandidateSuccess(
                        this.peerConnections[message.from]
                    ),
                this.onAddIceCandidateError(this.peerConnections[message.from])
            );
    };

    private handleTrackEvent = (peer: any, remoteStream: any) => (
        event: any
    ) => {
        remoteStream[peer] = event.stream;
        remoteStream[peer]
            .getTracks()
            .forEach((track: any) =>
                this.peerConnections[peer].addTrack(track, remoteStream[peer])
            );
        this.remoteVideo[peer].src = URL.createObjectURL(remoteStream[peer]);
    };

    private setVideoRef = (video: any) => {
        this.video = video;
    };

    private setRemoveVideoRef = (elm: any) => (video: any) => {
        console.log('this.remoteVideo[elm] = video;');
        this.remoteVideo[elm] = video;
    };

    private gotStream = (stream: any) => {
        this.video.src = URL.createObjectURL(stream);
        this.stream = stream;
    };

    private start = () => {
        console.log('Requesting local stream');
        navigator.mediaDevices
            .getUserMedia({
                audio: true,
                video: true
            })
            .then(this.gotStream)
            .then(() => {
                const channel = this.props.match.params.id;
                this.socket.send(
                    JSON.stringify({
                        channel,
                        type: 'REGISTERING',
                        userId: this.userID
                    })
                );
            })
            .catch(e => console.log(e));
    };

    private onIceStateChange = (pc: any) => (event: any) => {
        if (pc) {
            console.log(`ICE state: ${pc.iceConnectionState}`);
            console.log('ICE state change event: ', event);
        }
    };

    private onCreateAnswerSuccess = (
        desc: any,
        { from, to }: { from: string; to: string }
    ) => {
        console.log(`Offer from pc1, ${desc.sdp}`);
        console.log('pc1 setLocalDescription start');
        this.peerConnections[from]
            .setLocalDescription(desc)
            .then(
                this.onSetLocalSuccessAnswer(
                    desc,
                    from,
                    MESSAGE_TYPES.CREATEANSWER
                ),
                this.onSetSessionAnswerDescriptionError
            );
    };

    private onCreateOfferSuccess = (desc: any, from: string) => {
        console.log(`Offer from pc1, ${desc.sdp}`);
        console.log('pc1 setLocalDescription start');
        this.peerConnections[from]
            .setLocalDescription(desc)
            .then(
                () => this.onSetLocalSuccess(desc, MESSAGE_TYPES.CREATEOFFER),
                this.onSetSessionOfferDescriptionError
            );
    };

    private onSetLocalSuccess = (description: any, type: string) => {
        this.peers.forEach((peer: any) => {
            this.socket.send(
                JSON.stringify({
                    description,
                    from: this.userID,
                    to: peer,
                    type
                })
            );
        });
    };

    private onSetLocalSuccessAnswer = (
        description: any,
        to: string,
        type: string
    ) => {
        this.socket.send(
            JSON.stringify({
                description,
                from: this.userID,
                to,
                type
            })
        );
    };

    private onSetRemoteSuccess = (pc: any) => {
        console.log(`setRemoteDescription complete`, pc);
    };

    private onSetSessionOfferDescriptionError = (error: any) => {
        console.log(
            `Failed to set Offer session description: ${error.toString()}`
        );
    };
    private onSetSessionAnswerDescriptionError = (error: any) => {
        console.log(
            `Failed to set Answer session description: ${error.toString()}`
        );
    };

    private onSetSessionRemoteOfferDescriptionError = (error: any) => {
        console.log(
            `Failed to set remoteOffer session description: ${error.toString()}`
        );
    };

    private onCreateOfferSessionDescriptionError = (error: any) => {
        console.log(
            `Failed to create session description: ${error.toString()}`
        );
    };
    private onCreateAnswerSessionDescriptionError = (error: any) => {
        console.log(
            `Failed to create session description: ${error.toString()}`
        );
    };

    private onIceCandidate = (from: string, to: string) => (event: any) => {
        this.socket.send(
            JSON.stringify({
                candidate: event.candidate,
                from,
                to,
                type: MESSAGE_TYPES.ICECANDIDATE
            })
        );
        console.log(
            `ICE candidate:\n${
                event.candidate ? event.candidate.candidate : '(null)'
            }`
        );
    };

    private onAddIceCandidateSuccess = (pc: any) => {
        console.log(`addIceCandidate success`);
    };

    private onAddIceCandidateError = (pc: any) => (error: any) => {
        console.log(` failed to add ICE Candidate: ${error.toString()}`);
    };
}

export default Call;
