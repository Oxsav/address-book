/**
 * class Contact
 */
class Contact {
    public groupId: string;
    public name: string;
    public phone: string;
    public pictureUrl: string;

    /**
     * class Contact constructor
     * @param groupId {string} - groupid where the contacts should belong
     * @param name {string} - name of the contact
     * @param phone {string} - phone of the contact
     * @param pictureUrl {string} - picture url of the contact
     */
    constructor(
        groupId: string,
        name: string,
        phone: string,
        pictureUrl: string
    ) {
        this.groupId = groupId;
        this.name = name;
        this.phone = phone;
        this.pictureUrl = pictureUrl;
    }
}
